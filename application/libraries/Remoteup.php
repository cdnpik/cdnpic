<?php
// not soo good code.. will improve later

class Remoteup {

var $source;
var $save_to;
var $set_extension;
var $quality;
var $name;
var $image_width;
var $image_height;
var $image_size_str;
var $orig_name;

function download($method = 'gd')
{
$info = @GetImageSize($this->source);
$mime = $info['mime'];


if(!$mime) exit('Please Make sure that the remote file is actually a valid image.');

$this->image_width = $info['0'];
$this->image_height = $info['1'];
$this->image_size_str = $info['3'];
$this->orig_name = basename($this->source);

$type = substr(strrchr($mime, '/'), 1);

switch ($type) 
{
case 'jpeg':
    $image_create_func = 'ImageCreateFromJPEG';
    $image_save_func = 'ImageJPEG';
	$new_image_ext = 'jpg';
	
	// Best Quality: 100
	$quality = isSet($this->quality) ? $this->quality : 100; 
    break;

case 'png':
    $image_create_func = 'ImageCreateFromPNG';
    $image_save_func = 'ImagePNG';
	$new_image_ext = 'png';
	
	// Compression Level: from 0  (no compression) to 9
	$quality = isSet($this->quality) ? $this->quality : 0;
    break;

case 'bmp':
    $image_create_func = 'ImageCreateFromBMP';
    $image_save_func = 'ImageBMP';
	$new_image_ext = 'bmp';
    break;

case 'gif':
    $image_create_func = 'ImageCreateFromGIF';
    $image_save_func = 'ImageGIF';
	$new_image_ext = 'gif';
    break;


default: 
	$image_create_func = 'ImageCreateFromJPEG';
    $image_save_func = 'ImageJPEG';
	$new_image_ext = 'jpg';
}

if(isSet($this->set_extension))
{
$ext = strrchr($this->source, ".");
$strlen = strlen($ext);
$new_name = $this->name.'.'.$new_image_ext;
}
else
{
//$new_name = basename($this->source);
$new_name = $this->name.'.'.$new_image_ext;
}

$save_to = $this->save_to.$new_name;

    if($method == 'curl')
	{
    $save_image = $this->LoadImageCURL($save_to,$new_name);
	}
	elseif($method == 'gd')
	{
	$img = $image_create_func($this->source);

	    if(isSet($quality))
	    {
		   $save_image = $image_save_func($img, $save_to, $quality);
		}
		else
		{
		   $save_image = $image_save_func($img, $save_to);
		}
	}
	
	if($save_image)
	{
		$data['file_name'] = $this->name.'.'.$new_image_ext;
		$data['full_path'] = $this->save_to.$this->name.'.'.$new_image_ext;
		$data['orig_name'] = $this->orig_name;
		$data['image_width'] = $this->image_width;
		$data['image_height'] = $this->image_height;
		$data['image_size_str'] = $this->image_size_str;
		return $data;
	}
}

function LoadImageCURL($save_to,$new_name)
{
$ch = curl_init($this->source);
$fp = fopen($save_to, "wb");

// set URL and other appropriate options
$options = array(CURLOPT_FILE => $fp,
                 CURLOPT_HEADER => 0,
                 CURLOPT_FOLLOWLOCATION => 1,
	             CURLOPT_TIMEOUT => 60); // 1 minute timeout (should be enough)

curl_setopt_array($ch, $options);

$save = curl_exec($ch);
curl_close($ch);
fclose($fp);

if($save)
{
	return $new_name;
}
}
}
?>
