<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 *	 @access public
	 * Maps to the following URL
	 * 		http://cdnpic.com/index.php/welcome
	 *	- or -  
	 * 		http://cdnpic.com/index.php/welcome/index
	 *	- or -
	 * 	- or -
	 *		http://cdnpic.com/welcome/
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 */
	public function index()
	{
		$this->load->view('upload/ajax/start');
	}

	/**
	*	Displays the image
	* 
	* 	@access public
	*
	*	This method takes the name of the image as argument and 
	*	displays the view which will be having the image
	*
	*
	**/
	public function v($code=0)
	{
		if (!$code) {
			echo "Hun?";
		}
		else{
			$code = $this->uri->segment(2);
			$data['clean_code'] = $this->security->sanitize_filename($code);
                        $this->load->view('v/single_view',$data);
		}
	}
	/**
	*	Displays Contact Email
	*	
	*	@access public
	*	
	*	@todo Contact Form
	*
	**/
	public function contact()
	{
		$this->load->view('pages/contact');
	}

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
