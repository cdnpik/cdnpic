<?php

class Legacy extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
	}

	function index() {
		$this->load->view('upload/legacy/upload_form', array('error' => ' '));
	}

	function do_upload() {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '5024';
		$config['encrypt_name'] = 'TRUE';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {
			// Upload Was Not Successful => Show The Upload Form View With Errors
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload/legacy/upload_form', $error);
		} else {
			// Upload Was Successful , Grab The Upload Details To $uploaded Array
			$uploaded = $this->upload->data();

			// Create Thumbnail Only If It's An Image
			if ($uploaded['is_image'] == 1) {
				$config['image_library'] = 'GD2';
				$config['source_image'] = $uploaded['full_path'];
				$config['new_image'] = './uploads/thumbs/';
				$config['master_dim'] = 'width';
				$config['quality'] = 100;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 190;
				$config['height'] = 190;
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
			}
			$data['name'] = $uploaded['file_name'];
			$this->load->view('upload/legacy/upload_success', $data);
		}
	}
}
?>
