<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CDNPic - Contact Us</title>
<meta name="description" content="Awesome Image Hosting Service">
<link rel="stylesheet" href="/assets/css/bootstrap_legacy.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css">
<link rel="icon" type="image/png" href="/assets/img/favicon.png">
<!--[if lt IE 7]>
<link rel="stylesheet" href="/assets/css/bootstrap-ie6.min.css">
<![endif]-->
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>

</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">CDNPic</a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li><a href="/contact/">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
 <div class="alert alert-info">        
Contact via email.
</div>
PUT YOUR EMAIL HERE
<br />
<br />
    <div class="well">
        <h3>Notes</h3>
 		<ul>
        <li>The maximum file size for uploads is <strong>4 MB</strong>.</li>
        <li>Only images can be uploaded</li>
        </ul>
    </div>
</div>
</body> 
</html>
