<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CDNPic- Uploaded Your Images</title>
<meta name="description" content="Awesome Image Hosting Service">
<link rel="stylesheet" href="/assets/css/bootstrap_legacy.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css">
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<!--[if lt IE 7]>
<link rel="stylesheet" href="/assets/css/bootstrap-ie6.min.css">
<![endif]-->
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
   <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">CDNPic</a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li><a href="/contact/">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
<div class="alert alert-success">        
Image was uploaded succefully
</div>

<div class="row">
  <div class="span4">
            <img src="/uploads/thumbs/<?php echo $name; ?>" alt="">
            </div>
</div>
<br />
<div class="tabbable tabs-bottom">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#l">Link</a></li>
          <li class=""><a data-toggle="tab" href="#h">HTML</a></li>
          <li class=""><a data-toggle="tab" href="#b">BBCode</a></li>
        </ul>
        <div class="tab-content">
          <div id="l" class="tab-pane active">
            <p>
              <pre>http://cdnpic.com/v/<?php echo $name; ?></pre>
            </p>
          </div>
          <div id="h" class="tab-pane">
            <p><pre>
<?php  
$html_op = htmlspecialchars("<a href=\"http://cdnpic.com/v/$name\"><img src=\"http://cdnpic.com/uploads/thumbs/$name\" alt=\"\" title=\"Hosted by cdnpic.com\" /></a>",ENT_QUOTES);
echo $html_op;
?>
            </pre></p>
          </div>
          <div id="b" class="tab-pane">
            <p><pre>
[URL="http://cdnpic.com/v/<?php echo $name; ?>"][IMG]http://cdnpic.com/uploads/thumbs/<?php echo $name; ?>[/IMG][/URL]            </pre></p>
          </div>
        </div>
</div> <!-- /tabbable -->

    <br>
    <br>
    <div class="well">
        <h3>Notes</h3>
        <ul>
        <li>Use the <strong>HTML </strong>Code to share your image in blogs,etc.</li>
        <li>Use the <strong>BB </strong>Code to share your image in Forums.</li>
        </ul>
    </div>
</div>
</body> 
</html>
