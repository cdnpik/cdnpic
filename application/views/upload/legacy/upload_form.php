<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CDNPic - Easy Image Hosting</title>
<meta name="description" content="Awesome Image Hosting Service">
<link rel="stylesheet" href="/assets/css/bootstrap_legacy.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css">
<link rel="icon" type="image/png" href="/assets/img/favicon.png">
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<!--[if lt IE 7]>
<link rel="stylesheet" href="/assets/css/bootstrap-ie6.min.css">
<![endif]-->
<!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">CDNPic</a>
            <div class="nav-collapse">
                <ul class="nav">
                    <li><a href="/contact/">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container">
 <div class="alert alert-info">        
Welcome to CDNPic.
</div>
    <br>
<?php 
if (strlen($error)>1)
{
	echo '<div class="alert alert-error">';
	echo '<a class="close" data-dismiss="alert">×</a>';
	echo '<strong>'.$error.'</strong>';
	echo '</div>';
} 
?>

<div class="tabbable tabs-bottom">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#computer">Computer</a></li>
        </ul>
<div class="tab-content">
          <div id="computer" class="tab-pane active">
        			<?php echo form_open_multipart('upload/legacy/do_upload'); ?>
				<input type="file" name="userfile" class="input-file"/>
				<br />
				<p class="help-block">Please select the image that you would like to upload</p>
				<br />
				<input type="submit" value="Upload" class="btn btn-large"/>
				</form> 
          </div>

</div>
</div>
		

    <br>
    <div class="well">
        <h3>Notes</h3>
 		<ul>
        <li>The maximum file size for uploads is <strong>4 MB</strong>.</li>
        <li>Only images can be uploaded</li>
        </ul>
    </div>
</div>
</body> 
</html>
